package com.skava;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.skava.mapper.ProductExtractMapper;


@SpringBootApplication
public class HadoopMapReduceApplication extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(HadoopMapReduceApplication.class, args);
		int exitCode = ToolRunner.run(new HadoopMapReduceApplication(), args);
		System.exit(exitCode);
	}

	public int run(String[] args) throws Exception {
		Scan scan  = getScanObject();		
		Configuration config = HBaseConfiguration.create();
	
		Job job = new Job(config,"sample");
		job.setJarByClass(HadoopMapReduceApplication.class);
		job.setJobName("hbaseDataExtracter");
		TableMapReduceUtil.initTableMapperJob(
			    "my-table",        // input table
				scan,               // Scan instance to control CF and attribute selection
				ProductExtractMapper.class,     // mapper class
				ImmutableBytesWritable.class,
	            IntWritable.class,
	            job);
		
        job.setOutputFormatClass(NullOutputFormat.class);
        job.setNumReduceTasks(0);
        
        int returnValue = job.waitForCompletion(true) ? 0:1;
         
        if(job.isSuccessful()) {
            System.out.println("Job was successful");
        } else if(!job.isSuccessful()) {
            System.out.println("Job was not successful");           
        }
         
        return returnValue;

	}

	private Scan getScanObject() {
		Scan scan = new Scan();
		scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
		scan.setCacheBlocks(false); 
		
		return scan;

	}

}
