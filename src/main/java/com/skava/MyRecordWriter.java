package com.skava;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

public class MyRecordWriter extends RecordWriter {

	private KafkaTemplate<Integer, String> kafkaTemplate;
	private String topic;

	MyRecordWriter(TaskAttemptContext tac) {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		ProducerFactory<Integer, String> pf = new DefaultKafkaProducerFactory<Integer, String>(props);
		this.kafkaTemplate = new KafkaTemplate<>(pf);

		Configuration config;
		config = tac.getConfiguration();
		topic = config.get("Topic");

	}

	@Override
	public void write(Object k, Object v) throws IOException, InterruptedException {
		this.kafkaTemplate.send(this.topic, k.toString());
	}

	@Override
	public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
		this.kafkaTemplate.flush();
	}

}
