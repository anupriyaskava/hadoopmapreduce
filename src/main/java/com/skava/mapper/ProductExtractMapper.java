package com.skava.mapper;

import java.util.List;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;


public class ProductExtractMapper extends TableMapper<ImmutableBytesWritable, IntWritable> {

    protected void map(ImmutableBytesWritable key, Result value, Context context) throws java.io.IOException, InterruptedException {
    	System.out.println(key.toString()+","+ Bytes.toString(key.get()));
    	System.out.println("**************************************************************************************");
    	List<Cell> keyValueList = value.listCells();
    	System.out.println(keyValueList.toString());
    	System.out.println("**************************************************************************************");
    	System.out.println(context.toString());
    	System.out.println("**************************************************************************************");
    }
}