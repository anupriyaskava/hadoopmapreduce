package com.skava;

import java.io.IOException;

import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.OutputCommitter;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

public class MyOutputFormat extends OutputFormat {

	@Override
	public RecordWriter getRecordWriter(TaskAttemptContext tac) throws IOException, InterruptedException {
		return new MyRecordWriter(tac);
	}

	@Override
	public void checkOutputSpecs(JobContext jc) throws IOException {

	}

	@Override
	public OutputCommitter getOutputCommitter(TaskAttemptContext tac) throws IOException, InterruptedException {
		return new MyOutputCommitter();
	}

}