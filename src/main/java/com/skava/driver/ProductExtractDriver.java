package com.skava.driver;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.springframework.boot.SpringApplication;

import com.skava.HadoopMapReduceApplication;
import com.skava.mapper.ProductExtractMapper;
import com.skava.util.jobUtil;


public class ProductExtractDriver extends Configured implements Tool{
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new ProductExtractDriver(), args);
		System.exit(exitCode);
	}
	
	public int run(String[] args) throws Exception {
		Scan scan  = jobUtil.getScanObject();		
		Configuration config = HBaseConfiguration.create();
	
		Job job = new Job(config,"ProductDataExtracter");
		job.setJarByClass(HadoopMapReduceApplication.class);
		job.setJobName("ProductDataExtracter");
		TableMapReduceUtil.initTableMapperJob(
			    "my-table",        // input table
				scan,               // Scan instance to control CF and attribute selection
				ProductExtractMapper.class,     // mapper class
				ImmutableBytesWritable.class,
	            IntWritable.class,
	            job);
		
        job.setOutputFormatClass(NullOutputFormat.class);
        job.setNumReduceTasks(0);
        
        int returnValue = job.waitForCompletion(true) ? 0:1;
         
        if(job.isSuccessful()) {
            System.out.println("Job was successful");
        } else if(!job.isSuccessful()) {
            System.out.println("Job was not successful");           
        }
         
        return returnValue;

	}



}
