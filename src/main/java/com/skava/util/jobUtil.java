package com.skava.util;

import org.apache.hadoop.hbase.client.Scan;

public class jobUtil {
	
	/**
	 * Scan object for hadoop table scan
	 * @return scan
	 */
	public static Scan getScanObject() {
		Scan scan = new Scan();
		scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
		scan.setCacheBlocks(false); 	
		return scan;
	}

}
