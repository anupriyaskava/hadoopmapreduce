### Hadoop MapReduce with Kafka
This is sample code describes Mapreduce with Kafka.

**Class Description**
- **HadoopMapReduceApplication** - Starting class where job configured
- **MapClass** - Mapper class
- **ReducerClass** - Reducer class
- Custom Output Classes
**MyOutputFormat** , **MyRecordWriter**,  **MyOutputCommitter** - custom output format to write mapper/reducer output

**Input**
- input.txt 

**Steps to Run**
- Open new terminal start Apache Zookeeper
> C:\kafka_2.12-0.10.2.1>.\bin\windows\zookeeper-server-start.bat .\config\zookeeper.propertiesC:\kafka_2.12-0.10.2.1>.\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties

- Open new terminal start Apache Kafka

> C:\kafka_2.12-0.10.2.1>.\bin\windows\kafka-server-start.bat .\config\server.properties


- Open new terminal and start the consumer listening to the testtopic - (topic name may change)
> C:\kafka_2.12-0.10.2.1>.\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic testtopic --from-beginning

- Start Application 

**Output**
Below will be output in Consumer 
cat
bat
cat
boy
bat

**Note**
If Reducer is needed kindly include below line in run method of HadoopMapReduceApplication and remove   job.setNumReduceTasks(0);
> job.setReducerClass(ReducerClass.class);

**Reference**
https://dzone.com/articles/magic-of-kafka-with-spring-boot
https://my-bigdata-blog.blogspot.com/2017/07/hadoop-custom-outputformat-hdfs-send-to-kafka.html
https://examples.javacodegeeks.com/enterprise-java/apache-hadoop/hadoop-hello-world-example/
